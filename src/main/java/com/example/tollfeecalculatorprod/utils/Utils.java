package com.example.tollfeecalculatorprod.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class Utils {
    private Utils() {
    }
    public static LocalDateTime dateToLocalDateTime(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
    public static Date localDateTimeToDate(LocalDateTime localDateTime){
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    public static LocalDate[] convertToDateArray(Date[] dates) {
        LocalDate[] localDates = new LocalDate[dates.length];

        for (int i = 0; i < dates.length; i++) {
            localDates[i] = dates[i].toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
        }
        return localDates;
    }
    public static List<Date[]> getListsOfDatesGroupedByDay(Date... dates) {
        Map<LocalDate, List<Date>> dateMap = Arrays.stream(dates)
                .collect(Collectors.groupingBy(
                        date -> date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                        Collectors.toList()
                ));
        // Convert the Map values to arrays and add them to the separateDatesByDayList list
        List<Date[]> separateDatesByDayList = new ArrayList<>();
        for (List<Date> dateList : dateMap.values()) {
            separateDatesByDayList.add(dateList.toArray(new Date[0]));
        }
        return separateDatesByDayList;
    }
    public static   int getHour(Date date) {
        return date.getHours();
    }
}
