package com.example.tollfeecalculatorprod.service;

import com.example.tollfeecalculatorprod.configuration.HolidaysConfig;
import com.example.tollfeecalculatorprod.configuration.WeekendConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;


@Service
public class FreeDatesService {
    @Autowired
    private HolidaysConfig holidaysConfig;
    @Autowired
    private WeekendConfig weekendConfig;
    public boolean isTollFreeDate(LocalDate date) {
        DayOfWeek dayOfWeek = date.getDayOfWeek();
        if (weekendConfig.getWeekend().contains(dayOfWeek)) {
            return true;
        }
        return holidaysConfig.getTollFreeDates().contains(date);
    }
}
