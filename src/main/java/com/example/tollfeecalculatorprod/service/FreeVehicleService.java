package com.example.tollfeecalculatorprod.service;

import com.example.tollfeecalculatorprod.model.interfaces.Vehicle;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FreeVehicleService {
    @Value("${free.vehicles}")
    private String freeVehicles;
    public boolean isTollFreeVehicle(Vehicle vehicle) {
        List<String> freeVehiclesType = List.of(freeVehicles.split(","));
        if(vehicle == null) return false;
        for (String vehicleType : freeVehiclesType) {
            if (vehicleType.equals( vehicle.getType())) {
                return true;
            }
        }
        return false;
    }
}
