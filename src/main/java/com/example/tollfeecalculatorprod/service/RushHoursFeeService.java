package com.example.tollfeecalculatorprod.service;


import com.example.tollfeecalculatorprod.configuration.RushHoursFeesConfig;
import com.example.tollfeecalculatorprod.model.RushHoursFee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;

@Service
public class RushHoursFeeService {
    @Autowired
    RushHoursFeesConfig rushHoursFee;
    public int getTollFeeForTime(LocalTime time) {
        for (RushHoursFee rushHoursFee:rushHoursFee.getRushHoursFeeList()){
            if( (time.isAfter(rushHoursFee.getStartTime()) || time.equals(rushHoursFee.getStartTime()) )
                    && ( time.isBefore(rushHoursFee.getEndTime()) || time.equals(rushHoursFee.getEndTime()) ) ){
                return rushHoursFee.getFee();
            }
        }
        return 0;
    }
}
