package com.example.tollfeecalculatorprod.service;



import com.example.tollfeecalculatorprod.model.interfaces.Vehicle;
import com.example.tollfeecalculatorprod.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TollCalculator {
    FreeVehicleService freeVehiclesService;
    FreeDatesService freeDatesService;

    RushHoursFeeService rushHoursFeeService;

    @Autowired
    public TollCalculator(FreeVehicleService freeVehiclesService, FreeDatesService freeDatesService, RushHoursFeeService rushHoursFeeService) {
        this.freeVehiclesService = freeVehiclesService;
        this.freeDatesService = freeDatesService;
        this.rushHoursFeeService = rushHoursFeeService;
    }

    public  int getTotalTollFee(Vehicle vehicle, Date... dates) {
          int result=0;
          if (freeVehiclesService.isTollFreeVehicle(vehicle) || dates.length==0){
            return result;
          }

          List<Date[]> datesList = Utils.getListsOfDatesGroupedByDay(dates);

          for (Date[] dateArray : datesList) {
            result+= getTotalTollFeeForOneDay(dateArray);
          }
          return result;
        }

      public int getTotalTollFeeForOneDay(Date... dates) {
        if(freeDatesService.isTollFreeDate(Utils.dateToLocalDateTime(dates[0]).toLocalDate())) return 0;
        Map<Date, Integer> feesForAllDates = new HashMap<>();

        for (Date date : dates) {
          int fee = rushHoursFeeService.getTollFeeForTime(Utils.dateToLocalDateTime(date).toLocalTime());
           feesForAllDates.put(date,fee);
      }
        int totalFee= removeLowerFeesInCaseOfMultiFeesInSameHour(feesForAllDates);

        if (totalFee > 60) totalFee = 60;
        return totalFee;
      }

      private  int removeLowerFeesInCaseOfMultiFeesInSameHour(Map<Date, Integer> totalFeesMap) {
          Map<Integer, Map.Entry<Date, Integer>> maxFeesPerHourMap = totalFeesMap.entrySet().stream()
                  .collect(Collectors.groupingBy(
                          entry -> Utils.getHour(entry.getKey()),
                          Collectors.collectingAndThen(
                                  Collectors.maxBy(Map.Entry.comparingByValue()),
                                  entry -> entry.orElse(null)
                          )
                  ));

          return maxFeesPerHourMap.values().stream().mapToInt(Map.Entry::getValue).sum();
      }

}

