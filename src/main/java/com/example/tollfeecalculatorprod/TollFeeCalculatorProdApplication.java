package com.example.tollfeecalculatorprod;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TollFeeCalculatorProdApplication {

    public static void main(String[] args) {
        SpringApplication.run(TollFeeCalculatorProdApplication.class, args);
    }

}
