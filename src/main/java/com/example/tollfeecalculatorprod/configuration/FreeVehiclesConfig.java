package com.example.tollfeecalculatorprod.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class FreeVehiclesConfig {
    @Value("${free.vehicles}")
    private String freeVehicles;

    public List<String> getFreeVehiclesType() {
        return Arrays.stream(freeVehicles.split(","))
                .map(String::trim)
                .collect(Collectors.toList());
    }
}
