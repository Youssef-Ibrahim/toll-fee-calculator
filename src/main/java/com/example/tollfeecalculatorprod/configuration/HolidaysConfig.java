package com.example.tollfeecalculatorprod.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
@Configuration
public class HolidaysConfig {
//    private static final int TOLL_FREE_YEAR = 2013;
    //protected static final Set<LocalDate> tollFreeDates = new HashSet<>();

public HolidaysConfig(){

}
//    public static Set<LocalDate> setHolidays(){
//        int TOLL_FREE_YEAR = 2013;
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 1, 1));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 3, 28));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 3, 29));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 4, 1));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 4, 30));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 5, 1));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 5, 8));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 5, 9));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 6, 5));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 6, 6));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 6, 21));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 7, 1));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 11, 1));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 12, 24));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 12, 25));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 12, 26));
//        tollFreeDates.add(LocalDate.of(TOLL_FREE_YEAR, 12, 31));
//        return tollFreeDates;
//    }


    @Value("${myapp.toll-free-dates}")
    private String tollFreeDates;

    public Set<LocalDate> getTollFreeDates() {
        String[] dateStrings = tollFreeDates.split(",");
//        String[] dateStrings = {"1-1", "3-28", "3-29", "4-1", "4-30", "5-1", "5-8", "5-9", "6-5", "6-6", "6-21", "7-1", "11-1", "12-24", "12-25", "12-26", "12-31"};
        Set<LocalDate> tollFreeDates = new HashSet<>();

        for (String dateString : dateStrings) {
            dateString = dateString.trim();
            String[] dateParts = dateString.split("-");
            int month = Integer.parseInt(dateParts[0]);
            int day = Integer.parseInt(dateParts[1]);
            LocalDate now = LocalDate.now();
            int year= now.getYear();
            LocalDate date = LocalDate.of(year, month, day);
            tollFreeDates.add(date);
        }

        return tollFreeDates;
    }
}
