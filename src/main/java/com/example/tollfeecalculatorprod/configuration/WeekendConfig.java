package com.example.tollfeecalculatorprod.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Configuration
public class WeekendConfig {
   @Value("${weekend}")
   private String weekend;
   private static Set<DayOfWeek> Days = new HashSet<>();

    public  Set<DayOfWeek> getWeekend() {
        List<String> weekendDays = List.of(weekend.split(","));
        Days=weekendDays.stream().map(d->d.toUpperCase()).map(day->DayOfWeek.valueOf(day)).collect(Collectors.toSet());
        return Days;
    }

}
