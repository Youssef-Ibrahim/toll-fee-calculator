package com.example.tollfeecalculatorprod.configuration;


import com.example.tollfeecalculatorprod.model.RushHoursFee;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
@Configuration
public class RushHoursFeesConfig {
  public RushHoursFeesConfig() {}
    public static List<RushHoursFee> setRushHoursFee(){
        List<RushHoursFee>  rushHoursFeeList=new ArrayList<>();
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(6,0),LocalTime.of(6,29),8));
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(6,30),LocalTime.of(6,59),13));
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(7,0),LocalTime.of(7,59),18));
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(8,0),LocalTime.of(8,29),13));
        //rushHoursFeeList.add(new RushHoursFee(LocalTime.of(8,30),LocalTime.of(14,59),8));// not like original func
        for (int hour = 8; hour <= 14; hour++) {
            LocalTime startTime = LocalTime.of(hour, 30);
            LocalTime endTime = LocalTime.of(hour, 59);
            int fee = 8;
            rushHoursFeeList.add(new RushHoursFee(startTime, endTime, fee));
        }
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(15,0),LocalTime.of(15,29),13));
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(15,30),LocalTime.of(16,59),18));
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(17,0),LocalTime.of(17,59),13));
        rushHoursFeeList.add(new RushHoursFee(LocalTime.of(18,0),LocalTime.of(18,29),8));
        return rushHoursFeeList;
    }

    @Value("${myapp.rush-hours-fee-list}")
    private String rushHoursFeeListString;

    public List<RushHoursFee> getRushHoursFeeList() {
        String[] feeStrings = rushHoursFeeListString.split(",");
        List<RushHoursFee> rushHoursFeeList = new ArrayList<>();

        for (String feeString : feeStrings) {
            feeString = feeString.trim();
            String[] parts = feeString.split("-");
            LocalTime startTime = LocalTime.parse(parts[0]);
            LocalTime endTime = LocalTime.parse(parts[1]);
            int fee = Integer.parseInt(parts[2]);

            RushHoursFee rushHoursFee = new RushHoursFee(startTime, endTime, fee);
            rushHoursFeeList.add(rushHoursFee);
        }

        return rushHoursFeeList;
    }
}
