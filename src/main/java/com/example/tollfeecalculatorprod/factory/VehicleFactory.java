package com.example.tollfeecalculatorprod.factory;



import com.example.tollfeecalculatorprod.model.Car;
import com.example.tollfeecalculatorprod.model.Motorbike;
import com.example.tollfeecalculatorprod.model.interfaces.Vehicle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class VehicleFactory {
    public static Vehicle getVehicleByType(String type) {
        try {
            if (type != null) {
                switch (type){
                    case "Motorbike":
                        return new Motorbike();
                    case "Car":
                        return new Car();
                    default:
                        return new Vehicle() {
                            @Override
                            public String getType() {
                                return type;
                            }
                        };
                }

            }
        } catch (Exception e) {
           e.printStackTrace();
        }
        return new Car();
    }

}
