package com.example.tollfeecalculatorprod.model;

import java.time.LocalTime;

public class RushHoursFee {
    private LocalTime startTime;
    private LocalTime endTime;
    int fee;

    public RushHoursFee() {
    }

    public RushHoursFee(LocalTime startTime, LocalTime endTime, int fee) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.fee = fee;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }
}
