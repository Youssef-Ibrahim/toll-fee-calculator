package com.example.tollfeecalculatorprod.model;


import com.example.tollfeecalculatorprod.model.interfaces.Vehicle;

public class Motorbike implements Vehicle {
  @Override
  public String getType() {
    return "Motorbike";
  }
}
