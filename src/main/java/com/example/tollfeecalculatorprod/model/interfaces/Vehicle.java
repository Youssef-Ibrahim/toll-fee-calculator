package com.example.tollfeecalculatorprod.model.interfaces;

public interface Vehicle {

  String getType();
}
