package com.example.tollfeecalculatorprod;

//import com.main.fawryassignment.service.TollCalculator;


import com.example.tollfeecalculatorprod.factory.VehicleFactory;
import com.example.tollfeecalculatorprod.model.Car;
import com.example.tollfeecalculatorprod.model.Motorbike;
import com.example.tollfeecalculatorprod.model.interfaces.Vehicle;
import com.example.tollfeecalculatorprod.service.FreeDatesService;
import com.example.tollfeecalculatorprod.service.FreeVehicleService;
import com.example.tollfeecalculatorprod.service.RushHoursFeeService;
import com.example.tollfeecalculatorprod.service.TollCalculator;
import com.example.tollfeecalculatorprod.utils.Utils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(classes = TollFeeCalculatorProdApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TollCalculatorTests {
    @Autowired
    public TollCalculator tollCalculator;
    @Autowired
    RushHoursFeeService rushHoursFeeService;
    @Autowired
    FreeDatesService freeDatesService;
    @Autowired
    FreeVehicleService freeVehicleService;

    @BeforeAll
    public static void setup(){

    }
    @Test
    void testIsTollFreeDate() {
        assertTrue(freeDatesService.isTollFreeDate(LocalDate.of(2023,9,3)));
        assertFalse(freeDatesService.isTollFreeDate(LocalDate.of(2023,9,5)));
    }

    @Test
    void testTollFreeVehicles() {
        assertFalse(freeVehicleService.isTollFreeVehicle(VehicleFactory.getVehicleByType("Car")));
        assertTrue(freeVehicleService.isTollFreeVehicle(VehicleFactory.getVehicleByType("Motorbike")));
    }
    @Test
    void testGetTollFeeForOneDate(){
        assertEquals(8, rushHoursFeeService.getTollFeeForTime(LocalTime.of(6, 0, 0)));
        assertEquals(13, rushHoursFeeService.getTollFeeForTime( LocalTime.of(6, 30, 0)));
        assertEquals(18, rushHoursFeeService.getTollFeeForTime( LocalTime.of(7, 0, 0)));

    }

    @Test
    void testGetTotalTollFee() {
        Date date5= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,6,45,0)) ;//13
        Date date6= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,10,0)) ;//13
        Date date7= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,9,30,0)) ;//8
        Date date8= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,15,0,0)) ;//13
        int totalFee=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Car"),date5,date8,date7,date6);
        assertEquals(47,totalFee);
    }

    @Test
    void testGetTotalTollFeeWithCrossHours(){
        Date date1= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,9,7,15,0)) ;//18
        Date date5= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,9,7,46,0)) ;//18
        Date date2= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,10,0)) ;//13
        Date date3= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,30,0)) ;//8
        Date date4= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,15,0,0)) ;//13
        int totalFee=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Car"),date1,date4,date3,date2,date5);
        assertEquals(44,totalFee);

        int totalFee4=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Motorbike"));
        assertEquals(0,totalFee4);

    }




    @Test
    void testGetTotalTollForMulitebleDays() {
        Date date4= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,9,7,45,0)) ;//18
        Date date9= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,9,7,45,0)) ;//18-
        Date date1= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,10,0)) ;//13
        Date date2= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,30,0)) ;//8-
        Date date3= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,15,0,0)) ;//13
        Date date5= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,17,59,0)) ;//13
        Date date6= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,18,58,0)) ;//0
        Date date7= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,17,6,40,0)) ;//13
        Date date8= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,22,6,50,0)) ;//13
        int totalFee=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Car"),date4,date1,date2,date3,date5,date8,date7,date6,date9);
        assertEquals(83,totalFee);
    }
    @Test
    void testGetTotalTollFeeWhenTollFeeMoreThan60(){
        Date date9= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,6,28,0)) ;//8
        Date date10= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,7,29,0)) ;//18
        Date date11= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,29,0)) ;//13
        Date date12= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,15,0,0)) ;//13
        Date date13= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,16,1,0)) ;//18
        Date date14= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,17,5,0)) ;//13
        int totalFee=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Car"),date9,date10,date11,date12,date13,date14);
        assertEquals(60,totalFee);
    }
    @Test
    void testGetTotalTollFeeWithFreeVehicleType(){
        Date date1= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,7,45,0)) ;//18
        Date date2= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,10,0)) ;//13
        Date date3= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,8,30,0)) ;//8
        Date date4= Utils.localDateTimeToDate(LocalDateTime.of(2013,5,10,15,0,0)) ;//13
        int totalFee=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Motorbike"),date1,date4,date3,date2);
        assertEquals(0,totalFee);
    }
    @Test
    void testGetTotalTollFeeInHoliday(){
        Date date1= Utils.localDateTimeToDate(LocalDateTime.of(2023,1,1,7,45,0)) ;//18
        Date date2= Utils.localDateTimeToDate(LocalDateTime.of(2023,6,5,8,10,0)) ;//13
        Date date3= Utils.localDateTimeToDate(LocalDateTime.of(2023,6,6,8,30,0)) ;//8
        Date date4= Utils.localDateTimeToDate(LocalDateTime.of(2023,12,31,15,0,0)) ;//13
        int totalFee=tollCalculator.getTotalTollFee(VehicleFactory.getVehicleByType("Car"),date1,date4,date3,date2);
        assertEquals(0,totalFee);
    }


}
